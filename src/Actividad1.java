
public class Actividad1 implements Runnable {

    private int sleepingTime = 0;

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            sleepingTime = (int) (Math.random() * 5000);
            System.out.println("Thread : " + i);

            try {
                Thread.sleep(sleepingTime);
            } catch (InterruptedException ie) {
                System.out.println("Thread interrupted! " + ie);
            }
        }

        System.out.println("Thread finished!");
    }

    public static void main(String[] args) {
        Thread t = new Thread(new Actividad1());
        t.run();
    }
}
